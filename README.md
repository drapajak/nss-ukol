# NSS Úkol

Vytvořil Jakub Drápalík

Webová aplikace pro rezervaci fotbalových hřišť

## Použité technologie

- HTML
- CSS
- Vue.js
- SpringBoot
- PostgreSQL

## Popis

Jedná se o webovou aplikaci, kde majitelé fotbalových hřišť mohou pronajímat svá hřiště pro různé amaterské zápasy (například v rámci Hanspaulky) či pro jednotlivce. Hráči a týmy budou mít naopak možnost taková hřiště vyhledávat a případně si na ně udělat rezervaci.

## Nastavení / Instalace

V rámci aplikace jsou rozlišeny dvě role, a to sice hráč a majitel hřiště

### Hráč

- V horním vyhledávacím poli je možné nastavit filtrování dle lokace hřiště, ceny za pronájem 
- Při rozkliknutí detailu hřiště je možné pomocí tlačítka "Rezervovat" přejít na modul kalendáře, kde lze nastavit datum a čas pro danou rezervaci
- Po vybrání data rezervace je hráč přesměrován na platební bránu

### Majitel hřiště

- Majitel hřiště může na stránku přidat nové hřiště, včetně jeho lokace, ceny za pronájem, fotografií apod.
- V profilu pak uvidí statistiky ohledně profitability a vytíženosti jednotlivých hřišť

## Licencování

Všechna práva vyhrazena

&copy;Jakub Drápalík 2024
